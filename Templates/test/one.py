class UserSignup(CommonBaseHandler):
    def check_username(self, user_name):
        cursor_u = self.db.cursor()
        user_sql = query_obj.check_username % user_name
        username_check = cursor_u.execute(user_sql)
        cursor_u.close()
        return username_check

    def check_email(self, email_id):
        cursor_e = self.db.cursor()
        chk_email = query_obj.check_email % email_id
        e = cursor_e.execute(chk_email)
        cursor_e.close()
        return e

    def get(self):
        self.render("sign_up.html")

    def post(self):
        signup_successful = False
        self.set_headers()
        response_data = ""
        user_name = self.get_argument("user_name")
        email = self.get_argument("user_email")
        password = self.get_argument("password")
        firstname = self.get_argument("first_name", default="NULL", strip=False)
        lastname = self.get_argument("last_name", default="NULL", strip=False)
        # v_money = self.get_argument("virtual_money", default=100, strip=False)
        dob = self.get_argument("d_o_b", default="NULL", strip=False)
        gender = self.get_argument("gender", default="NULL", strip=False)
        country = self.get_argument("country", default="NULL", strip=False)

        # get value of profile-image
        profile_photo = self.get_argument("profile-image", default="profile-default.png", strip=False)

        chk_user_name = self.check_username(user_name)

        if chk_user_name != 1:
            if not self.check_email(email):
                signup_successful = True
                cursor = self.db.cursor()
                # create_user_sql = query_obj.create_user % (user_name, email, password, firstname, lastname, 100, dob, gender, country, datetime.now())
                create_user_sql = query_obj.create_user % (
                user_name, email, password, firstname, lastname, 10000, dob, gender, country, datetime.now(),
                profile_photo)
                cursor.execute(create_user_sql)
                self.db.commit()
                user_id = cursor.lastrowid
                cursor.close()

                self.set_secure_cookie("qc_user", str(email))

#                r.set("user_session", user_name, user_id)

                response_data = json.dumps({
                    'success': {
                        'code': success_obj.success_code_created,
                        'message': success_obj.success_message_user_create,
                        'data': {"user_id": user_id}
                    }
                })

            else:
                response_data = json.dumps({
                    'error': {
                        'code': error_obj.error_code_resource_exists,
                        'message': error_obj.error_message_email_id_exists
                    }
                })
        else:
            response_data = json.dumps({
                'error': {
                    'code': error_obj.error_code_resource_exists,
                    'message': error_obj.error_message_username_exists
                }
            })

        # self.write(response_data)
        # self.finish()
        if signup_successful:
            self.redirect('/user/login')
        else:
            self.write(response_data)






class errors():
    def __init__(self):
        self.error_message_empty_field = "Email or password is empty"
        self.error_message_email_id_exists = "Email_id already exists"
        self.error_message_username_exists = "User_name already exists"
        self.error_message_empty_field = "Email or password is empty"


class success():
    def __init__(self):
        self.success_message_user = "Login Successfull."
        self.success_message_user_create = "User created successfully"




class SQLqueries():
    def __init__(self):
        self.login_email_check = "SELECT * FROM user" \
            " WHERE email = '%s' AND deleted_at IS NULL"
        self.login_pass_check = "Select * FROM user " \
            "WHERE email = '%s' AND password = SHA2('%s',256) AND deleted_at IS NULL"


email_sql = "INSERT INTO %s ( %s ) VALUES ( %s )" % (table_name, columns, placeholders)