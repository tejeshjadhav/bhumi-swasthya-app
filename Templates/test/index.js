$(document).ready(function(){
            $("#signup_btn").click(function(){
                $.ajax({
                 type: "POST",
                 url: "https://localhost/user/create",
                 data: "user_name": $('#u_name').val(),
                       "user_email": $('#u_email').val(),
                       "password": $('#u_pass').val(),
                       "first_name": $('#u_first').val(),
                       "last_name": $('#u_last').val(),
                       "nick_name": $('#u_nick').val(),
                       "d_o_b": $('#u_dob').val(),
                       "gender": $('gender').val(),
                       "country": $('#u_country').val(),
                       "time_zone": $('#u_zone').val(),
                       "user_type": $('user_type').val(),
                       "referral_type": $('referral_type').val(),
                       "referral_data": $('referral_data').val(),

                 error: function(xhr,status, errorThrown ){
                        if (xhr.status === 0) {
                             alert('Not connected.\n Verify Network. [0]');
                         } else if (xhr.status == 404) {
                             alert('Requested page not found. [404]');
                         } else if (xhr.status == 500) {
                             alert('Internal Server Error [500].');
                         } else if (errorThrown === 'timeout') {
                             alert('Time out error.');
                         } else if (errorThrown === 'abort') {
                             alert('Ajax request aborted.');
                         } else {
                             alert('Uncaught Error.\n' + xhr.responseText);
                        }
                });
            });
        });